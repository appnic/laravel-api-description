<?php


namespace appnic\ApiDescription\Contracts;

use appnic\ApiDescription\Description;

interface Describable
{
    public function describe(Description $description);
}