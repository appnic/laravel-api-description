<?php

namespace appnic\ApiDescription\Resources;

use appnic\ApiDescription\Contracts\Describable;
use appnic\ApiDescription\Description;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class DescriptionResource extends JsonResource
{
    public function toResponse($request)
    {
        $response = parent::toResponse($request);

        if($this->resource instanceof Describable) {
            $this->resource->describe($description = new Description());
            $data = json_decode(json_encode($response->getData()), true); // TODO: is there a better way to convert stdObject to array?
            Arr::set($data, config('apidescription.key'), $description->toArray());
            $response->setData($data);
        }
        return $response;
    }
}
