<?php

namespace appnic\ApiDescription\Traits;

trait HasMeta
{
    public $meta = [];

    public function meta(string $key, $value) {
        $this->meta[$key] = $value;
        return $this;
    }
}