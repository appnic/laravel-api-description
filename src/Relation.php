<?php

namespace appnic\ApiDescription;

class Relation
{
    public $url;
    public $labelField;
    public $many = false;

    public function url(string $url) {
        $this->url = $url;
        return $this;
    }

    public function labelField(string $labelField) {
        $this->labelField = $labelField;
        return $this;
    }

    public function many(bool $many = true) {
        $this->many = $many;
        return $this;
    }

    public function toArray() {
        return [
            'url' => $this->url,
            'labelField' => $this->labelField,
            'many' => $this->many
        ];
    }
}