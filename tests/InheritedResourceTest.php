<?php

namespace appnic\ApiDescription\Tests;


use appnic\ApiDescription\Resources\DescriptionResource;
use Illuminate\Support\Arr;
use Orchestra\Testbench\TestCase;

class InheritedResourceTest extends TestCase
{
    public function getPackageProviders($app)
    {
        return ['appnic\ApiDescription\Providers\ApiDescriptionProvider'];
    }

    public function testResponseMetaContainsDescriptionFields()
    {
        $model = new TestModel([
            'name' => 'test',
            'type_id' => 1
        ]);

        $responseContent = json_decode((new InheritedResource($model))->response()->content(), true);
        $description = Arr::get($responseContent, 'meta');

        $this->assertArrayHasKey('additionalKey', $description);
        $this->assertArrayHasKey('description', $description);
    }

}